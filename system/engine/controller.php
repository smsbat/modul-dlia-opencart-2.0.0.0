<?php
abstract class Controller {
	protected $registry;

    protected function smsbat_init(){

        # Load language
        $this->load->language('module/smsbat');

        $this->registry->set('smsbat_logger', new Log('smsbat.log'));

        if ((($this->config->get('smsbat_login') &&
                    $this->config->get('smsbat_password'))||$this->config->get('smsbat_key')) &&
            file_exists(DIR_SYSTEM . 'library/smsbat_gateway.php')){

            # Load SmsBat library
            require_once(DIR_SYSTEM . 'library/smsbat_gateway.php');

            $gateway = new SmsBatGateway(
                $this->config->get('smsbat_login'),
                $this->config->get('smsbat_password'),
                $this->config->get('smsbat_key')
            );

            # Set sign
            $smsbat_sign = $this->config->get('smsbat_sign');
            $gateway->setSign($smsbat_sign);

            # Add to global registry
            $this->registry->set('smsbat_gateway', $gateway);
            return true;
        }
    }

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}