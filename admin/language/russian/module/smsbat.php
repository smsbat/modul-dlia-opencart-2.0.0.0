<?php

// Heading
$_['heading_title'] = 'SmsBat.com';
$_['text_module']   = 'Модули';
$_['text_edit']   = 'Редактировать модуль SmsBat.com';

$_['smsbat_saved_success'] = 'Успешно сохранены настройки';
$_['smsbat_smssend_success'] = 'Сообщение успешно отправлено на шлюз';

// Error
$_['smsbat_error_permission'] = 'Вы не имеете полномочий для изменения настроек данного модуля!';
$_['smsbat_error_request'] = 'Ошибка запроса';
$_['smsbat_error_auth_info'] = 'Необходимо задать идентификационный данные SMS-шлюза';
$_['smsbat_error_login_field'] = 'Необходимо указать логин';
$_['smsbat_error_password_field'] = 'Необходимо указать пароль';
$_['smsbat_error_sign_field'] = 'Необходимо указать подпись для сообщений';
$_['smsbat_error_admphone_field'] = 'Укажите номер телефона администратора';
$_['smsbat_error_sign_to_large'] = 'Подпись слишком длинная. Максимум 11 символов.';
$_['smsbat_error_empty_frmsms_message'] = 'Необходимо указать текст сообщения';
$_['smsbat_error_frmsms'] = 'Ошибка с отправкой сообщения';

// Tabs name in view
$_['smsbat_tab_connection'] = 'Настройки шлюза';
$_['smsbat_tab_signature'] = 'Подпись сообщений';
$_['smsbat_tab_events'] = 'Выполнять при действиях';
$_['smsbat_tab_about'] = 'О модуле';
$_['smsbat_tab_sendsms'] = 'Отправить SMS';

// Text messges
$_['smsbat_text_gate_settings'] = 'Настройки шлюза';
$_['smsbat_text_login'] = 'Логин';
$_['smsbat_text_login_placeholder'] = 'Логин (телефон) с SmsBat.com';
$_['smsbat_error_login'] = 'Телефон пустой или имеет неправильный формат. Должен быть как +380112223344';
$_['smsbat_text_password'] = 'Пароль';
$_['smsbat_error_password'] = 'Пароль не может быть пустым !';
$_['smsbat_text_key'] = 'API ключ';
$_['smsbat_error_key'] = 'Пустой API ключ';
$_['smsbat_text_sign'] = 'Подпись сообщений';
$_['smsbat_error_sign'] = 'Пустая подпись или несоответствующая формату. Должна быть не длинее 11 символов латиницей !';
$_['smsbat_text_admphone'] = 'Телефон администратора';
$_['smsbat_error_admphone'] = 'Телефон администратора пустой или имеет неправильный формат. Должен быть как +380112223344';
$_['smsbat_text_phone'] = 'Телефон получателя';
$_['smsbat_error_phone'] = 'Телефон получателя пустой или имеет неправильный формат. Должен быть как +380112223344';
$_['smsbat_text_notify_sms_to_admin'] = 'Сообщать по событиям администратора';
$_['smsbat_text_notify_sms_to_customer'] = 'Сообщать по событиям покупателя';
$_['smsbat_text_connection_established'] = 'Соеденение с SMS-шлюзом установлено';
$_['smsbat_text_connection_error'] = 'Нет связи с SMS-шлюзом';
$_['smsbat_events_admin_new_customer'] = 'Новый покупатель зарегистрирован';
$_['smsbat_events_admin_new_order'] = 'Осуществили новый заказ';
$_['smsbat_events_admin_new_email'] = 'Поступило новое письмо с контактной формы магазина';
$_['smsbat_text_frmsms_message'] = 'Текст сообщения';
$_['smsbat_error_message'] = 'Пустое сообщение';
$_['smsbat_text_frmsms_phone'] = 'Номер получателя';
$_['smsbat_text_button_send_sms'] = 'Отправить SMS';
$_['smsbat_events_admin_gateway_connection_error'] = 'Уведомлять на email при неудачном соединении со шлюзом';
$_['smsbat_events_customer_new_order_status'] = 'Изменение статуса заказа';
$_['smsbat_events_customer_new_order'] = 'Покупателю сообщение о новом заказе';
$_['smsbat_events_customer_new_register'] = 'Успешное завершение регистрации';

$_['smsbat_message_customer_new_order_status'] = 'Статус заказа #%s изменен на "%s"';

$_['smsbat_text_connection_tab_description'] =
'Укажите правильные данные для подключения к шлюзу SmsBat.com через HTTP/HTTPS протокол.<br/>';

$_['smsbat_text_about_tab_description'] =
'<b>%s &copy; %s Все права защищены</b><br/>
<br/>
Модуль предназначен для рассылки SMS уведомлений посредством шлюза SmsBat.com.
<br/><br/>
Данное произведение распространяется на основании BSD лицензии<br/><br/>
Текущая версия: %s<br/>';

# vi:ts=2:sw=2:ai:et:ft=php:enc=utf8
