<?php

$_['smsbat_saved_success'] = 'Налашутвання успішно збережено';

$_['smsbat_message_connection_error'] = 'Виникли неполадки зі шлюзом SmsBat під час пересилання SMS повідомлення.<br />Час відправки: %s<br />Відповідь серверу: %s';
$_['smsbat_message_customer_new_register'] = 'Вітаємо з успішною реєстрацією в інтернет-магазині "%s"';
$_['smsbat_message_customer_new_order'] = 'Дякуюємо за замовлення. Ваш номер #%s';
$_['smsbat_message_admin_new_customer'] = 'Зареєстровано нового покупця';
$_['smsbat_message_admin_new_order'] = 'Нове замовлення';
$_['smsbat_message_admin_new_email'] = 'Надіслано нове повідомлення із сторінки контактів';

$_['smsbat_page_head_title'] = 'Модуль SmsBat';

$_['smsbat_message_customer_new_order_status'] = 'Статус замовлення #%s змінився на "%s".';