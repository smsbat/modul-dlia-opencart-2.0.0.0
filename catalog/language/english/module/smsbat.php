<?php

$_['smsbat_saved_success'] = 'Success saved settings';

$_['smsbat_message_connection_error'] = 'When you send an alert via SMS could not string any Gateway.<br />Sending time: %s<br />Server response: %s';
$_['smsbat_message_customer_new_register'] = 'Congratulations on your successful registration in the store "%s".';
$_['smsbat_message_customer_new_order'] = 'Thank you for your purchase. Your order number #%s';
$_['smsbat_message_admin_new_order'] = 'A new order is performed.';
$_['smsbat_message_admin_new_customer'] = 'The new customer is registered.';
$_['smsbat_message_admin_new_email'] = 'A new letter is sent from the contact page.';

$_['smsbat_page_head_title'] = 'SmsBat module';

$_['smsbat_message_customer_new_order_status'] = 'Order status #%s is changed on "%s".';